---
Title: imhex
Homepage: https://github.com/WerWolv/ImHex
Repository: https://gitlab.com/kalilinux/packages/imhex
Architectures: amd64 arm64
Version: 1.32.2-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### imhex
 
  This package contains a Hex Editor for Reverse Engineers, Programmers and
  people who value their retinas when working at 3 AM.
 
 **Installed size:** `66.41 MB`  
 **How to install:** `sudo apt install imhex`  
 
 {{< spoiler "Dependencies:" >}}
 * libbz2-1.0
 * libc6 
 * libcurl4 
 * libdbus-1-3 
 * libfmt9 
 * libfreetype6 
 * libgcc-s1 
 * libglfw3 
 * liblzma5 
 * libmagic1 
 * libmbedcrypto7 
 * libopengl0
 * libstdc++6 
 * libyara10 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### imhex
 
 
 
 - - -
 
 ##### imhex-updater
 
 
 ```
 root@kali:~# imhex-updater -h
 [05:29:10] [INFO]  [updater]    Updater started with version type: -h
 [05:29:10] [INFO]  [updater]    Detected OS String: 
 [05:29:10] [ERROR] [updater]    Failed to detect installation type
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
