---
Title: 7zip
Homepage: https://www.7-zip.org/
Repository: https://salsa.debian.org/debian/7zip
Architectures: any
Version: 23.01+dfsg-8
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-linux-nethunter kali-tools-forensics kali-tools-respond 
Icon: images/7zip-logo.svg
PackagesInfo: |
 ### 7zip
 
  7-Zip is a file archiver with a high compression ratio. The main features
  of 7-Zip are:
   * High compression ratio in 7z format with LZMA and LZMA2 compression
   * Supported formats:
       * Packing / unpacking: 7z, XZ, BZIP2, GZIP, TAR, ZIP and WIM
       * Unpacking only: AR, ARJ, CAB, CHM, CPIO, CramFS, DMG, EXT, FAT,
           GPT, HFS, IHEX, ISO, LZH, LZMA, MBR, MSI, NSIS, NTFS, QCOW2,
           RPM, SquashFS, UDF, UEFI, VDI, VHD, VMDK, WIM, XAR and Z.
   * For ZIP and GZIP formats, 7-Zip provides a compression ratio that is
       2-10 % better than the ratio provided by PKZip and WinZip
   * Strong AES-256 encryption in 7z and ZIP formats
   * Powerful command line version
   
  "7zip" provides:
   * /usr/bin/7z:  Full featured with plugin functionality.
   * /usr/bin/7za: Major formats/features only.
   * /usr/bin/7zr: LZMA (.7z, .lzma, .xz) only. Minimal executable.
   
  Note: The unRAR code was dropped to keep compatible with DFSG.
        Install 7zip-rar pacakge in non-free section to use RAR files.
 
 **Installed size:** `6.52 MB`  
 **How to install:** `sudo apt install 7zip`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libgcc-s1 
 * libstdc++6 
 {{< /spoiler >}}
 
 ##### 7z
 
 7-Zip file archiver with a high compression ratio
 
 ```
 root@kali:~# 7z -h
 
 7-Zip 23.01 (x64) : Copyright (c) 1999-2023 Igor Pavlov : 2023-06-20
  64-bit locale=C.UTF-8 Threads:8 OPEN_MAX:1024
 
 Usage: 7z <command> [<switches>...] <archive_name> [<file_names>...] [@listfile]
 
 <Commands>
   a : Add files to archive
   b : Benchmark
   d : Delete files from archive
   e : Extract files from archive (without using directory names)
   h : Calculate hash values for files
   i : Show information about supported formats
   l : List contents of archive
   rn : Rename files in archive
   t : Test integrity of archive
   u : Update files to archive
   x : eXtract files with full paths
 
 <Switches>
   -- : Stop switches and @listfile parsing
   -ai[r[-|0]]{@listfile|!wildcard} : Include archives
   -ax[r[-|0]]{@listfile|!wildcard} : eXclude archives
   -ao{a|s|t|u} : set Overwrite mode
   -an : disable archive_name field
   -bb[0-3] : set output log level
   -bd : disable progress indicator
   -bs{o|e|p}{0|1|2} : set output stream for output/error/progress line
   -bt : show execution time statistics
   -i[r[-|0]]{@listfile|!wildcard} : Include filenames
   -m{Parameters} : set compression Method
     -mmt[N] : set number of CPU threads
     -mx[N] : set compression level: -mx1 (fastest) ... -mx9 (ultra)
   -o{Directory} : set Output directory
   -p{Password} : set Password
   -r[-|0] : Recurse subdirectories for name search
   -sa{a|e|s} : set Archive name mode
   -scc{UTF-8|WIN|DOS} : set charset for console input/output
   -scs{UTF-8|UTF-16LE|UTF-16BE|WIN|DOS|{id}} : set charset for list files
   -scrc[CRC32|CRC64|SHA1|SHA256|*] : set hash function for x, e, h commands
   -sdel : delete files after compression
   -seml[.] : send archive by email
   -sfx[{name}] : Create SFX archive
   -si[{name}] : read data from stdin
   -slp : set Large Pages mode
   -slt : show technical information for l (List) command
   -snh : store hard links as links
   -snl : store symbolic links as links
   -sni : store NT security information
   -sns[-] : store NTFS alternate streams
   -so : write data to stdout
   -spd : disable wildcard matching for file names
   -spe : eliminate duplication of root folder for extract command
   -spf[2] : use fully qualified file paths
   -ssc[-] : set sensitive case mode
   -sse : stop archive creating, if it can't open some input file
   -ssp : do not change Last Access Time of source files while archiving
   -ssw : compress shared files
   -stl : set archive timestamp from the most recently modified file
   -stm{HexMask} : set CPU thread affinity mask (hexadecimal number)
   -stx{Type} : exclude archive type
   -t{Type} : Set type of archive
   -u[-][p#][q#][r#][x#][y#][z#][!newArchiveName] : Update options
   -v{Size}[b|k|m|g] : Create volumes
   -w[{path}] : assign Work directory. Empty path means a temporary directory
   -x[r[-|0]]{@listfile|!wildcard} : eXclude filenames
   -y : assume Yes on all queries
 ```
 
 - - -
 
 ##### 7za
 
 7-Zip file archiver with a high compression ratio
 
 ```
 root@kali:~# 7za -h
 
 7-Zip (a) 23.01 (x64) : Copyright (c) 1999-2023 Igor Pavlov : 2023-06-20
  64-bit locale=C.UTF-8 Threads:8 OPEN_MAX:1024
 
 Usage: 7za <command> [<switches>...] <archive_name> [<file_names>...] [@listfile]
 
 <Commands>
   a : Add files to archive
   b : Benchmark
   d : Delete files from archive
   e : Extract files from archive (without using directory names)
   h : Calculate hash values for files
   i : Show information about supported formats
   l : List contents of archive
   rn : Rename files in archive
   t : Test integrity of archive
   u : Update files to archive
   x : eXtract files with full paths
 
 <Switches>
   -- : Stop switches and @listfile parsing
   -ai[r[-|0]]{@listfile|!wildcard} : Include archives
   -ax[r[-|0]]{@listfile|!wildcard} : eXclude archives
   -ao{a|s|t|u} : set Overwrite mode
   -an : disable archive_name field
   -bb[0-3] : set output log level
   -bd : disable progress indicator
   -bs{o|e|p}{0|1|2} : set output stream for output/error/progress line
   -bt : show execution time statistics
   -i[r[-|0]]{@listfile|!wildcard} : Include filenames
   -m{Parameters} : set compression Method
     -mmt[N] : set number of CPU threads
     -mx[N] : set compression level: -mx1 (fastest) ... -mx9 (ultra)
   -o{Directory} : set Output directory
   -p{Password} : set Password
   -r[-|0] : Recurse subdirectories for name search
   -sa{a|e|s} : set Archive name mode
   -scc{UTF-8|WIN|DOS} : set charset for console input/output
   -scs{UTF-8|UTF-16LE|UTF-16BE|WIN|DOS|{id}} : set charset for list files
   -scrc[CRC32|CRC64|SHA1|SHA256|*] : set hash function for x, e, h commands
   -sdel : delete files after compression
   -seml[.] : send archive by email
   -sfx[{name}] : Create SFX archive
   -si[{name}] : read data from stdin
   -slp : set Large Pages mode
   -slt : show technical information for l (List) command
   -snh : store hard links as links
   -snl : store symbolic links as links
   -sni : store NT security information
   -sns[-] : store NTFS alternate streams
   -so : write data to stdout
   -spd : disable wildcard matching for file names
   -spe : eliminate duplication of root folder for extract command
   -spf[2] : use fully qualified file paths
   -ssc[-] : set sensitive case mode
   -sse : stop archive creating, if it can't open some input file
   -ssp : do not change Last Access Time of source files while archiving
   -ssw : compress shared files
   -stl : set archive timestamp from the most recently modified file
   -stm{HexMask} : set CPU thread affinity mask (hexadecimal number)
   -stx{Type} : exclude archive type
   -t{Type} : Set type of archive
   -u[-][p#][q#][r#][x#][y#][z#][!newArchiveName] : Update options
   -v{Size}[b|k|m|g] : Create volumes
   -w[{path}] : assign Work directory. Empty path means a temporary directory
   -x[r[-|0]]{@listfile|!wildcard} : eXclude filenames
   -y : assume Yes on all queries
 ```
 
 - - -
 
 ##### 7zr
 
 7-Zip file archiver with a high compression ratio
 
 ```
 root@kali:~# 7zr -h
 
 7-Zip (r) 23.01 (x64) : Igor Pavlov : Public domain : 2023-06-20
  64-bit locale=C.UTF-8 Threads:8 OPEN_MAX:1024
 
 Usage: 7zr <command> [<switches>...] <archive_name> [<file_names>...] [@listfile]
 
 <Commands>
   a : Add files to archive
   b : Benchmark
   d : Delete files from archive
   e : Extract files from archive (without using directory names)
   h : Calculate hash values for files
   i : Show information about supported formats
   l : List contents of archive
   rn : Rename files in archive
   t : Test integrity of archive
   u : Update files to archive
   x : eXtract files with full paths
 
 <Switches>
   -- : Stop switches and @listfile parsing
   -ai[r[-|0]]{@listfile|!wildcard} : Include archives
   -ax[r[-|0]]{@listfile|!wildcard} : eXclude archives
   -ao{a|s|t|u} : set Overwrite mode
   -an : disable archive_name field
   -bb[0-3] : set output log level
   -bd : disable progress indicator
   -bs{o|e|p}{0|1|2} : set output stream for output/error/progress line
   -bt : show execution time statistics
   -i[r[-|0]]{@listfile|!wildcard} : Include filenames
   -m{Parameters} : set compression Method
     -mmt[N] : set number of CPU threads
     -mx[N] : set compression level: -mx1 (fastest) ... -mx9 (ultra)
   -o{Directory} : set Output directory
   -p{Password} : set Password
   -r[-|0] : Recurse subdirectories for name search
   -sa{a|e|s} : set Archive name mode
   -scc{UTF-8|WIN|DOS} : set charset for console input/output
   -scs{UTF-8|UTF-16LE|UTF-16BE|WIN|DOS|{id}} : set charset for list files
   -scrc[CRC32|CRC64|SHA1|SHA256|*] : set hash function for x, e, h commands
   -sdel : delete files after compression
   -seml[.] : send archive by email
   -sfx[{name}] : Create SFX archive
   -si[{name}] : read data from stdin
   -slp : set Large Pages mode
   -slt : show technical information for l (List) command
   -snh : store hard links as links
   -snl : store symbolic links as links
   -sni : store NT security information
   -sns[-] : store NTFS alternate streams
   -so : write data to stdout
   -spd : disable wildcard matching for file names
   -spe : eliminate duplication of root folder for extract command
   -spf[2] : use fully qualified file paths
   -ssc[-] : set sensitive case mode
   -sse : stop archive creating, if it can't open some input file
   -ssp : do not change Last Access Time of source files while archiving
   -ssw : compress shared files
   -stl : set archive timestamp from the most recently modified file
   -stm{HexMask} : set CPU thread affinity mask (hexadecimal number)
   -stx{Type} : exclude archive type
   -t{Type} : Set type of archive
   -u[-][p#][q#][r#][x#][y#][z#][!newArchiveName] : Update options
   -v{Size}[b|k|m|g] : Create volumes
   -w[{path}] : assign Work directory. Empty path means a temporary directory
   -x[r[-|0]]{@listfile|!wildcard} : eXclude filenames
   -y : assume Yes on all queries
 ```
 
 - - -
 
 ##### p7zip
 
 Wrapper on 7-Zip file archiver with high compression ratio
 
 ```
 root@kali:~# p7zip -h
 Usage: /usr/bin/p7zip [options] [--] [ name ... ]
 
 Options:
     -c --stdout --to-stdout      output data to stdout
     -d --decompress --uncompress decompress file
     -f --force                   do not ask questions
     -k --keep                    keep original file
     -h --help                    print this help
     --                           treat subsequent arguments as file
                                  names, even if they start with a dash
 
 ```
 
 - - -
 
 ### 7zip-standalone
 
  7-Zip is a file archiver with a high compression ratio.
   
  "7zip-standalone" provides:
   * /usr/bin/7zz: Full featured except plugins, standalone executable.
        This means 7zz can't work with unRAR plugin that provided by 7zip-rar
        package. Use 7zip package for unRAR plugin.
   
  Note: If you want to create SFX archive, you also needs 7zip package for SFX
        stub module.
 
 **Installed size:** `2.48 MB`  
 **How to install:** `sudo apt install 7zip-standalone`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libgcc-s1 
 * libstdc++6 
 {{< /spoiler >}}
 
 ##### 7zz
 
 7-Zip file archiver with a high compression ratio (standalone)
 
 ```
 root@kali:~# 7zz -h
 
 7-Zip (z) 23.01 (x64) : Copyright (c) 1999-2023 Igor Pavlov : 2023-06-20
  64-bit locale=C.UTF-8 Threads:8 OPEN_MAX:1024
 
 Usage: 7zz <command> [<switches>...] <archive_name> [<file_names>...] [@listfile]
 
 <Commands>
   a : Add files to archive
   b : Benchmark
   d : Delete files from archive
   e : Extract files from archive (without using directory names)
   h : Calculate hash values for files
   i : Show information about supported formats
   l : List contents of archive
   rn : Rename files in archive
   t : Test integrity of archive
   u : Update files to archive
   x : eXtract files with full paths
 
 <Switches>
   -- : Stop switches and @listfile parsing
   -ai[r[-|0]]{@listfile|!wildcard} : Include archives
   -ax[r[-|0]]{@listfile|!wildcard} : eXclude archives
   -ao{a|s|t|u} : set Overwrite mode
   -an : disable archive_name field
   -bb[0-3] : set output log level
   -bd : disable progress indicator
   -bs{o|e|p}{0|1|2} : set output stream for output/error/progress line
   -bt : show execution time statistics
   -i[r[-|0]]{@listfile|!wildcard} : Include filenames
   -m{Parameters} : set compression Method
     -mmt[N] : set number of CPU threads
     -mx[N] : set compression level: -mx1 (fastest) ... -mx9 (ultra)
   -o{Directory} : set Output directory
   -p{Password} : set Password
   -r[-|0] : Recurse subdirectories for name search
   -sa{a|e|s} : set Archive name mode
   -scc{UTF-8|WIN|DOS} : set charset for console input/output
   -scs{UTF-8|UTF-16LE|UTF-16BE|WIN|DOS|{id}} : set charset for list files
   -scrc[CRC32|CRC64|SHA1|SHA256|*] : set hash function for x, e, h commands
   -sdel : delete files after compression
   -seml[.] : send archive by email
   -sfx[{name}] : Create SFX archive
   -si[{name}] : read data from stdin
   -slp : set Large Pages mode
   -slt : show technical information for l (List) command
   -snh : store hard links as links
   -snl : store symbolic links as links
   -sni : store NT security information
   -sns[-] : store NTFS alternate streams
   -so : write data to stdout
   -spd : disable wildcard matching for file names
   -spe : eliminate duplication of root folder for extract command
   -spf[2] : use fully qualified file paths
   -ssc[-] : set sensitive case mode
   -sse : stop archive creating, if it can't open some input file
   -ssp : do not change Last Access Time of source files while archiving
   -ssw : compress shared files
   -stl : set archive timestamp from the most recently modified file
   -stm{HexMask} : set CPU thread affinity mask (hexadecimal number)
   -stx{Type} : exclude archive type
   -t{Type} : Set type of archive
   -u[-][p#][q#][r#][x#][y#][z#][!newArchiveName] : Update options
   -v{Size}[b|k|m|g] : Create volumes
   -w[{path}] : assign Work directory. Empty path means a temporary directory
   -x[r[-|0]]{@listfile|!wildcard} : eXclude filenames
   -y : assume Yes on all queries
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
