---
Title: snort
Homepage: https://www.snort.org/
Repository: https://gitlab.com/kalilinux/packages/snort
Architectures: any all
Version: 3.1.78.0-0kali1
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### snort
 
  Snort is a libpcap-based packet sniffer/logger which can be used as a
  lightweight network intrusion detection system. It features rules-based
  logging and can perform content searching/matching in addition to
  detecting a variety of other attacks and probes, such as buffer
  overflows, stealth port scans, CGI attacks, SMB probes, and much more.
  Snort has a real-time alerting capability, with alerts being sent to
  syslog, a separate "alert" file, or even to a Windows computer via Samba.
   
  This package provides the plain-vanilla version of Snort.
 
 **Installed size:** `9.06 MB`  
 **How to install:** `sudo apt install snort`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser 
 * debconf  | debconf-2.0
 * libc6 
 * libdaq3 
 * libdumbnet1 
 * libgcc-s1 
 * libhwloc15 
 * libluajit-5.1-2  | libluajit2-5.1-2 
 * liblzma5 
 * libnuma1 
 * libpcap0.8 
 * libpcre3
 * libssl3 
 * libstdc++6 
 * logrotate
 * net-tools
 * rsyslog | system-log-daemon
 * snort-common 
 * snort-common-libraries 
 * snort-rules-default 
 * zlib1g 
 {{< /spoiler >}}
 
 ##### appid_detector_builder.sh
 
 
 ```
 root@kali:~# appid_detector_builder.sh -h
 Snort Application Id - Detector Creation Tool
 
 Enter below, the AppId string to be associated with the Detector.
 (e.g. "CNN.com", "Yahoo!", "Avira Download/Update", etc.)
 AppId strings MUST NOT INCLUDE tab, backslash, apostrophe, or double-quote.
 
 ```
 
 - - -
 
 ##### snort
 
 
 ```
 root@kali:~# snort -h
 
 Snort has several options to get more help:
 
 -? list command line options (same as --help)
 --help this overview of help
 --help-commands [<module prefix>] output matching commands
 --help-config [<module prefix>] output matching config options
 --help-counts [<module prefix>] output matching peg counts
 --help-limits print the int upper bounds denoted by max*
 --help-module <module> output description of given module
 --help-modules list all available modules with brief help
 --help-modules-json dump description of all available modules in JSON format
 --help-plugins list all available plugins with brief help
 --help-options [<option prefix>] output matching command line options
 --help-signals dump available control signals
 --list-buffers output available inspection buffers
 --list-builtin [<module prefix>] output matching builtin rules
 --list-gids [<module prefix>] output matching generators
 --list-modules [<module type>] list all known modules
 --list-plugins list all known modules
 --show-plugins list module and plugin versions
 
 --help* and --list* options preempt other processing so should be last on the
 command line since any following options are ignored.  To ensure options like
 --markup and --plugin-path take effect, place them ahead of the help or list
 options.
 
 Options that filter output based on a matching prefix, such as --help-config
 won't output anything if there is no match.  If no prefix is given, everything
 matches.
 
 Report bugs to bugs@snort.org.
 
 ```
 
 - - -
 
 ##### snort2lua
 
 
 ```
 root@kali:~# snort2lua -h
 Usage: snort2lua [OPTIONS]... -c <snort_conf> ...
 
 Converts the Snort configuration file specified by the -c or --conf-file
 options into a Snort++ configuration file
 
 
 
 Options:
 
 -?                  show usage
 -h                  this overview of snort2lua
 -a                  default option.  print all data
 -c <snort_conf>     The Snort <snort_conf> file to convert
 -d                  print the differences, and only the differences, between the
                     Snort and Snort++ configurations to the <out_file>
 -e <error_file>     output all errors to <error_file>
 -i                  if <snort_conf> file contains any <include_file> or
                     <policy_file> (i.e. 'include path/to/conf/other_conf'), do
                     NOT parse those files
 -m                  add a remark to the end of every converted rule
 -o <out_file>       output the new Snort++ lua configuration to <out_file>
 -q                  quiet mode. Only output valid configuration information to
                     the <out_file>
 -r <rule_file>      output any converted rule to <rule_file>
 -s                  when parsing <include_file>, write <include_file>'s rules to
                     <rule_file>. Meaningless if '-i' provided
 -t                  when parsing <include_file>, write <include_file>'s
                     information, excluding rules, to <out_file>. Meaningless if
                     '-i' provided
 -V                  Print the current Snort2Lua version
 --bind-wizard       Add default wizard to bindings
 --bind-port         Convert port bindings
 --conf-file         Same as '-c'. A Snort <snort_conf> file which will be
                     converted
 --dont-parse-includes  
                     Same as '-p'. if <snort_conf> file contains any
                     <include_file> or <policy_file> (i.e. 'include
                     path/to/conf/other_conf'), do NOT parse those files
 --dont-convert-max-sessions  
                     do not convert max_tcp, max_udp, max_icmp, max_ip to
                     max_session
 --error-file=<error_file>  
                     Same as '-e'. output all errors to <error_file>
 --help              Same as '-h'. this overview of snort2lua
 --ips-policy-pattern  Convert config bindings matching this path to ips policy
                     bindings
 --markup            print help in asciidoc compatible format
 --output-file=<out_file>  
                     Same as '-o'. output the new Snort++ lua configuration to
                     <out_file>
 --print-all         Same as '-a'. default option.  print all data
 --print-differences  Same as '-d'. output the differences, and only the
                     differences, between the Snort and Snort++ configurations to
                     the <out_file>
 --quiet             Same as '-q'. quiet mode. Only output valid configuration
                     information to the <out_file>
 --remark            same as '-m'.  add a remark to the end of every converted
                     rule
 --rule-file=<rule_file>  
                     Same as '-r'. output any converted rule to <rule_file>
 --single-conf-file  Same as '-t'. when parsing <include_file>, write
                     <include_file>'s information, excluding rules, to
                     <out_file>
 --single-rule-file  Same as '-s'. when parsing <include_file>, write
                     <include_file>'s rules to <rule_file>.
 --version           Same as '-V'. Print the current Snort2Lua version
 
 
 
 Required option:
 
 	A Snort configuration file to convert. Set with either '-c' or '--conf-file'
 
 
 Default values:
 	<out_file>   =  snort.lua
 	<rule_file>  =  <out_file> = snort.lua.  Rules are written to the 'local_rules' variable in the <out_file>
 	<error_file> =  snort.rej.  This file will not be created in quiet mode.
 
 ```
 
 - - -
 
 ##### u2boat
 
 Unified2 Binary Output & Alert Tool
 
 ```
 root@kali:~# man u2boat
 U2BOAT(1)                   General Commands Manual                  U2BOAT(1)
 
 NAME
        u2boat - Unified2 Binary Output & Alert Tool
 
 SYNOPSIS
        u2boat [-t type] <infile> <outfile>
 
 DESCRIPTION
        This  manual  page  documents  briefly the u2boat command.  This manual
        page was written for the Debian distribution because the original  pro-
        gram does not have a manual page.
 
        u2boat  is  a utility that converts Snort's Unified2 logs to other for-
        mats. It was introduced in Snort due to the lack of support for formats
        that were present in the Snort 2.8.x releases.  Unified2 logs to  other
        formats.
 
 OPTIONS
        -t type
               Type  specifies  the type of output that the program should cre-
               ate. The only current valid option is 'pcap'
 
 SEE ALSO
        snort (8)
 
 AUTHOR
        This program was written by Ryan Jordan.
 
        This manual page was  written  by  Javier  Fernandez-Sanguino  <jfs@de-
        bian.org>, for the Debian GNU/Linux system (but may be used by others).
 
                               12th December 2014                     U2BOAT(1)
 ```
 
 - - -
 
 ##### u2spewfoo
 
 Tool for dumping the contents of unified2 files to stdout
 
 ```
 root@kali:~# man u2spewfoo
 U2SPEWFOO(1)                General Commands Manual               U2SPEWFOO(1)
 
 NAME
        u2spewfoo -  tool for dumping the contents of unified2 files to stdout
 
 SYNOPSIS
        u2boat <infile>
 
 DESCRIPTION
        This  manual page documents briefly the u2spewfoo command.  This manual
        page was written for the Debian distribution because the original  pro-
        gram does not have a manual page.
 
        u2spewfoo  is  a  lightweight  tool for dumping the contents of Snort's
        Unified2 log files to stdout. In order to use it Snort first has to  be
        configured to use this format in its configuration file.
 
        The  tool will take the log file and dump the information on the events
        in Standard output. This information includes the  event  and  relevant
        information  about  it  (such  as  IP addresses and ports, the time the
        event was detected, etc.) as well as  the  packet  that  triggered  the
        event  (if  Snort has been configured to store a packet capture associ-
        ated with events).
 
 EXAMPLES
        To use it run it against a unified2  log  file  by  running:  u2spewfoo
        snort.log
 
        The following is a sample output of this tool:
 
        (Event)
            sensor id: 0    event id: 4 event second: 1299698138    event microsecond: 146591
            sig id: 1   gen id: 1   revision: 0  classification: 0
            priority: 0 ip source: 10.1.2.3 ip destination: 10.9.8.7
            src port: 60710 dest port: 80   protocol: 6 impact_flag: 0  blocked: 0
 
        Packet
            sensor id: 0    event id: 4 event second: 1299698138
            packet second: 1299698138   packet microsecond: 146591
            linktype: 1 packet_length: 54
        [    0] 02 09 08 07 06 05 02 01 02 03 04 05 08 00 45 00  ..............E.
        [   16] 00 28 00 06 00 00 40 06 5C B7 0A 01 02 03 0A 09  .(....@........
        [   32] 08 07 ED 26 00 50 00 00 00 62 00 00 00 2D 50 10  ...&.P...b...-P.
        [   48] 01 00 A2 BB 00 00                                ......
 
        (ExtraDataHdr)
            event type: 4   event length: 33
 
        (ExtraData)
            sensor id: 0    event id: 2 event second: 1299698138
            type: 9 datatype: 1 bloblength: 9   HTTP URI: /
 
        (ExtraDataHdr)
            event type: 4   event length: 78
 
        (ExtraData)
            sensor id: 0    event id: 2 event second: 1299698138
            type: 10    datatype: 1 bloblength: 12  HTTP Hostname: example.com
 
 SEE ALSO
        snort (8)
 
 AUTHOR
        This program was written by Adam Keeton.
 
        This  manual  page  was  written  by Javier Fernandez-Sanguino <jfs@de-
        bian.org>, for the Debian GNU/Linux system (but may be used by others).
 
                               12th December 2014                  U2SPEWFOO(1)
 ```
 
 - - -
 
 ### snort-common
 
  Snort is a libpcap-based packet sniffer/logger which can be used as a
  lightweight network intrusion detection system. It features rules-based
  logging and can perform content searching/matching in addition to
  detecting a variety of other attacks and probes, such as buffer
  overflows, stealth port scans, CGI attacks, SMB probes, and much more.
  Snort has a real-time alerting capability, with alerts being sent to
  syslog, a separate "alert" file, or even to a Windows computer via Samba.
   
  This is a common package which holds cron jobs, tools, and config files
  used by all the different package flavors.
 
 **Installed size:** `231 KB`  
 **How to install:** `sudo apt install snort-common`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser 
 * debconf  | debconf-2.0
 * dpkg 
 * perl
 {{< /spoiler >}}
 
 
 - - -
 
 ### snort-common-libraries
 
  Snort is a libpcap-based packet sniffer/logger which can be used as a
  lightweight network intrusion detection system. It features rules-based
  logging and can perform content searching/matching in addition to
  detecting a variety of other attacks and probes, such as buffer
  overflows, stealth port scans, CGI attacks, SMB probes, and much more.
  Snort has a real-time alerting capability, with alerts being sent to
  syslog, a separate "alert" file, or even to a Windows computer via Samba.
   
  This package provides libraries used by all the Snort binary packages.
 
 **Installed size:** `1.26 MB`  
 **How to install:** `sudo apt install snort-common-libraries`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 
 - - -
 
 ### snort-doc
 
  Snort is a libpcap-based packet sniffer/logger which can be used as a
  lightweight network intrusion detection system. It features rules-based
  logging and can perform content searching/matching in addition to
  detecting a variety of other attacks and probes, such as buffer
  overflows, stealth port scans, CGI attacks, SMB probes, and much more.
  Snort has a real-time alerting capability, with alerts being sent to
  syslog, a separate "alert" file, or even to a Windows computer via Samba.
   
  This package provides the documentation for Snort.
 
 **Installed size:** `1.50 MB`  
 **How to install:** `sudo apt install snort-doc`  
 
 
 - - -
 
 ### snort-rules-default
 
  Snort is a libpcap-based packet sniffer/logger which can be used as a
  lightweight network intrusion detection system. It features rules-based
  logging and can perform content searching/matching in addition to
  detecting a variety of other attacks and probes, such as buffer
  overflows, stealth port scans, CGI attacks, SMB probes, and much more.
  Snort has a real-time alerting capability, with alerts being sent to
  syslog, a separate "alert" file, or even to a Windows computer via Samba.
   
  This is the Snort default ruleset, which provides a basic set of network
  intrusion detection rules developed by the Snort community. They can be
  used as a basis for development of additional rules. Users using Snort to
  defend networks in production environments are encouraged to update their
  local rulesets as described in the included documentation or using the
  oinkmaster package.
 
 **Installed size:** `1.64 MB`  
 **How to install:** `sudo apt install snort-rules-default`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser 
 * debconf  | debconf-2.0
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
